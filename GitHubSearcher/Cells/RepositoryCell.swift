import UIKit

protocol RepositoryCellDelegate {
  func openTapped(_ cell: RepositoryCell)
}

class RepositoryCell: UITableViewCell {
  static let identifier = "RepositoryCell"
  
  @IBOutlet weak var repositoryNameLabel: UILabel!
  @IBOutlet weak var userNameLabel: UILabel!
  @IBOutlet weak var openButton: UIButton!
  
  var delegate: RepositoryCellDelegate?
  
  @IBAction func downloadTapped(_ sender: AnyObject) {
    delegate?.openTapped(self)
  }
  
  func configure(repository: Repository) {
    repositoryNameLabel.text = repository.repositoryName
    userNameLabel.text = repository.userLogin
  }
}
