import Foundation.NSURL

class Repository {
  let repositoryName: String
  let userLogin: String
  let url: String
  let index: Int
  
  init(repositoryName: String,
       userLogin: String,
       url: String,
       index: Int) {
    self.repositoryName = repositoryName
    self.userLogin = userLogin
    self.url = url
    self.index = index
  }
}
