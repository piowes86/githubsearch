import Foundation

class QueryService {
  let defaultSession = URLSession(configuration: .default)
  var dataTask: URLSessionDataTask?
  var errorMessage = ""
  var repositories: [Repository] = []
  
  typealias JSONDictionary = [String: Any]
  typealias QueryResult = ([Repository]?, String) -> Void
  
  func getSearchResults(searchTerm: String, completion: @escaping QueryResult) {
    dataTask?.cancel()
        
    if var urlComponents = URLComponents(string: "https://api.github.com/search/repositories") {
      urlComponents.query = "q=\(searchTerm)&sort=stars&order=desc"
     
      guard let url = urlComponents.url else { return }
      
      dataTask =
        defaultSession.dataTask(with: url) { [weak self] data, response, error in
        defer {
          self?.dataTask = nil
        }
        if let error = error {
          self?.errorMessage += "DataTask error: " +
                                  error.localizedDescription + "\n"
        } else if
          let data = data,
          let response = response as? HTTPURLResponse,
          response.statusCode == 200 {
          self?.updateSearchResults(data)
          DispatchQueue.main.async {
            completion(self?.repositories, self?.errorMessage ?? "")
          }
        }
      }
      dataTask?.resume()
    }
  }
  
  private func updateSearchResults(_ data: Data) {
    var response: JSONDictionary?
    repositories.removeAll()
    
    do {
      response = try JSONSerialization.jsonObject(with: data, options: []) as? JSONDictionary
    } catch let parseError as NSError {
      errorMessage += "JSONSerialization error: \(parseError.localizedDescription)\n"
      return
    }
    
    guard let arrayOfRepositories = response!["items"] as? [Any] else {
      errorMessage += "Dictionary does not contain results key\n"
      return
    }
    
    var index = 0
    
    for repositoryDictionary in arrayOfRepositories {
      if let repositoryDictionary = repositoryDictionary as? JSONDictionary,
        let repositoryName = repositoryDictionary["name"] as? String,
        let owner = repositoryDictionary["owner"] as? [String: Any],
        let userLogin = owner["login"] as? String,
        let repositoryUrl = repositoryDictionary["html_url"] as? String {
        repositories.append(Repository(repositoryName: repositoryName,
                                       userLogin: userLogin,
                                       url: repositoryUrl,
                                       index: index))
        index += 1
      } else {
        errorMessage += "Problem parsing of repositoryDictionary\n"
      }
    }
  }
}

